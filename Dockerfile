### STAGE 1: Build ###
FROM node:12.7-alpine AS build
WORKDIR /usr/src/app
COPY app/package.json app/package-lock.json ./
RUN npm install
COPY app/ .
RUN npm run build

### STAGE 2: Run ###
FROM nginx:1.17.1-alpine
COPY confs/nginx.conf /etc/nginx/nginx.conf
COPY --from=build /usr/src/app/dist/angular-starter /usr/share/nginx/html
RUN sed -i 's/nginx\:x\:101\:101\:nginx\:\/var\/cache\/nginx\:\/bin\/sh/nginx\:x\:101\:101\:nginx\:\/var\/cache\/nginx\:\/bin\/bash/g' /etc/passwd
EXPOSE 8888
USER nginx