# tw-todofrontend

## 1. How to start
```
$ docker-compose up -d


$ curl -v localhost/angular-starter/
```

## Deploy on Kubernetes file tw-todofrontend.yml

[Link do projeto com pipeline](https://gitlab.com/jonascavalcantineto/tw-todofrontend/pipelines)

